﻿1. The app.config file in the "data" folder contains all the webpage element identifiers.
	the format for saving the element is <add key="logo_Id" value="logo"/>
	the naming convention for the "key" is variableName_typeOfSelectorToFindTheElementBy
	"value" is the exact value/content retrieved from the webpage
	Example: 
	<add key="calculateButton_Xpath" value="/html/body/div[3]/div[1]/table[1]/tbody/tr/td/div/table/tbody/tr[4]/td/input"/>

2. The TakeScreenshot method in common > WebDriverFacade, has a fileName variable commented. This is the actual file name that should 
	be used so uncomment it and delete the other fileName variable that's being used.