﻿using OpenQA.Selenium;

namespace Calculator.Net.pageobjects
{
    public class AmortizationCalculatorPage
    {
        #region Element Declarations
        WebDriverFacade webDriverFacade;
        #endregion

        public AmortizationCalculatorPage(WebDriverFacade webDriverFacade)
        {
            this.webDriverFacade = webDriverFacade;
        }

        public void CalculateLoanAmortization(string loanAmount, string loanTerm, string interestRate)
        {
            EnterFieldValue(Elements.amorLoanAmountField, loanAmount);
            EnterFieldValue(Elements.amorLoanTermField, loanTerm);
            EnterFieldValue(Elements.amorInterestRateField, interestRate);
            webDriverFacade.ClickElement(Elements.amorCalculateButton);
        }

        public void EnterFieldValue(By element, string value)
        {
            webDriverFacade.ClearField(element);
            webDriverFacade.TypeInput(element, value);
        }
    }
}
