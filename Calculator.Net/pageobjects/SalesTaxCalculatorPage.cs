﻿using OpenQA.Selenium;

namespace Calculator.Net.pageobjects
{
    public class SalesTaxCalculatorPage
    {
        #region Elemet Declarations
        WebDriverFacade webDriverFacade;
        #endregion

        public SalesTaxCalculatorPage(WebDriverFacade webDriverFacade)
        {
            this.webDriverFacade = webDriverFacade;
        }

        public void CalculateAfterTaxPrice(string beforeTaxPrice, string salesTaxRate)
        {
            EnterFieldValue(Elements.beforeTaxField, beforeTaxPrice);
            EnterFieldValue(Elements.salesTaxRateField, salesTaxRate);
            ClickCalculate();
        }
        
        public void CalculateBeforeTaxPrice(string salesTaxRate, string afterTaxPrice)
        {
            webDriverFacade.ClearField(Elements.beforeTaxField);
            EnterFieldValue(Elements.salesTaxRateField, salesTaxRate);
            EnterFieldValue(Elements.afterTaxPriceField, afterTaxPrice);
            ClickCalculate();
        }
        
        public void CalculateSalesTaxRate(string beforeTaxPrice, string afterTaxPrice)
        {
            webDriverFacade.ClearField(Elements.salesTaxRateField);
            EnterFieldValue(Elements.beforeTaxField, beforeTaxPrice);
            EnterFieldValue(Elements.afterTaxPriceField, afterTaxPrice);
            ClickCalculate();
        }
        
        public void ClickCalculate()
        {
            webDriverFacade.ClickElement(Elements.salesTaxCalculateButton);
        }
        
        /// <summary>
        /// Clears the specified element field, then enters the
        /// specified value
        /// </summary>
        /// <param name="element">field to be cleared</param>
        /// <param name="value">value to be inserted into the field</param>
        public void EnterFieldValue(By element, string value)
        {
            webDriverFacade.ClearField(element);
            webDriverFacade.TypeInput(element, value);
        }
        
    }
}
