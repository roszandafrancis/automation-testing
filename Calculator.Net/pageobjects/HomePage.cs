﻿

namespace Calculator.Net.pageobjects
{
    public class HomePage
    {
        #region Elemet Declarations
        WebDriverFacade webDriverFacade;
        #endregion

        public HomePage(WebDriverFacade webDriverFacade)
        {
            this.webDriverFacade = webDriverFacade;
        }

        public void GoToSalesTaxCalculatorPage()
        {
            webDriverFacade.ClickElement(Elements.salesTaxCalculatorLink);
        }

        public void GoToAmoritizationCalculatorPage()
        {
            webDriverFacade.ClickElement(Elements.amortizationCalculatorLink);
        }
        
        public void GoToBmrCalculatorPage()
        {
            webDriverFacade.ClickElement(Elements.bmrCalculatorLink);
        }
        
        public void GoToPercentageCalculatorPage()
        {
            webDriverFacade.ClickElement(Elements.percentageCalculatorLink);
        }
        
        public void GoToAgeCalculatorPage()
        {
            webDriverFacade.ClickElement(Elements.ageCalculatorLink);
        }
    }
}
