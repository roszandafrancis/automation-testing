﻿using Calculator.Net.common;
using OpenQA.Selenium;
using System;

namespace Calculator.Net.pageobjects
{
    public class BMRCalculatorPage
    {
        #region Elemet Declarations

        WebDriverFacade webDriverFacade;
        private const int MINIMUM_AGE = 15;
        private const int MAXIMUM_AGE = 80;
        private const double ONE_FEET_IN_CM = 30.48;
        private const double ONE_INCH_IN_CM = 2.54;
        private const double ONE_POUND_IN_KG = 0.45359237;
        private const int KATMAC_ADD_VALUE = 370;
        private const double KATMAC_MULTIPLIER = 21.6;
        private const int KATMAC_SUBTRACTOR = 1;
        private const int PERCENTAGE = 100;
        private const int MIFFJEOR_WEIGHT_MULTIPLIER = 10;
        private const double MIFFJEOR_HEIGHT_MULTIPLIER = 6.25;
        private const int MIFFJEOR_AGE_MULTIPLIER = 5;
        private const int MIFFJEOR_MALE_AGE_ADDER = 5;
        private const int MIFFJEOR_FEMALE_AGE_ADDER = 161;
        private const double HARRISBEN_MALE_WEIGHT_MULTIPLIER = 13.397;
        private const double HARRISBEN_FEMALE_WEIGHT_MULTIPLIER = 9.247;
        private const double HARRISBEN_MALE_HEIGHT_MULTIPLIER = 4.799;
        private const double HARRISBEN_FEMALE_HEIGHT_MULTIPLIER = 3.098;
        private const double HARRISBEN_MALE_AGE_MULTIPLIER = 5.677;
        private const double HARRISBEN_FEMALE_AGE_MULTIPLIER = 4.330;
        private const double HARRISBEN_MALE_AGE_ADDER = 88.362;
        private const double HARRISBEN_FEMALE_AGE_ADDER = 447.593;

        #endregion

        public BMRCalculatorPage(WebDriverFacade webDriverFacade)
        {
            this.webDriverFacade = webDriverFacade;
        }

        #region Public Test Methods
        public void CalculateBmrInUsUnits(int age, Enums.Gender gender, string heightInFeet, string heightInInches, string weightInPounds)
        {
            webDriverFacade.ClickElement(Elements.usUnitsTab);
            EnterAge(age);
            SelectGender(gender);
            EnterFieldValue(Elements.heightInFeetField, heightInFeet);
            EnterFieldValue(Elements.heightInInchesField, heightInInches);
            EnterFieldValue(Elements.weightInPoundField, weightInPounds);
            webDriverFacade.ClickElement(Elements.bmrCalculateButton);
        }

        public void CalculateBmrInMetricUnits(int age, Enums.Gender gender, string heightInCm, string weightInKg)
        {
            webDriverFacade.ClickElement(Elements.metricUnitsTab);
            EnterAge(age);
            SelectGender(gender);
            EnterFieldValue(Elements.heightInCmField, heightInCm);
            EnterFieldValue(Elements.weightInKgField, weightInKg);
            webDriverFacade.ClickElement(Elements.bmrCalculateButton);
        }
        
        public void AdjustAllSettings(Enums.ResultsUnit resultUnit, Enums.EstimationFormula estimationFormula, string bodyFatPercentage)
        {
            webDriverFacade.ClickElement(Elements.settingsLink);
            SelectResultsUnit(resultUnit);
            SelectBmrEstimationFormula(estimationFormula, bodyFatPercentage);
        }

        public string CalculateTemperatureConversion(string valueToBeConverted, Enums.Temperature temperatureToBeConvertedFrom, Enums.Temperature temperatureToBeConvertedTo)
        {            
            webDriverFacade.ClickElement(Elements.bmrOtherUnitsTab);
            webDriverFacade.SwitchToIframe(0);
            webDriverFacade.ClickElement(Elements.bmrTemperatureTab);
            EnterFieldValue(Elements.bmrFromField, valueToBeConverted);
            SelectFromDDL(Elements.bmrFromDDL, temperatureToBeConvertedFrom.ToString());
            return SelectFromDDL(Elements.bmrToDDL, temperatureToBeConvertedTo.ToString());
        }
        #endregion

        #region Public helper methods

        public void EnterFieldValue(By element, string value)
        {
            webDriverFacade.ClearField(element);
            webDriverFacade.TypeInput(element, value);
        }

        public string SelectFromDDL(By element, string valueInDDL)
        {
            var result = "";
            var listOfValues = webDriverFacade.InitalizeDDLForSelection(element);

            foreach (var option in listOfValues.Options)
            {
                if (option.Text.Contains(valueInDDL))
                {
                    result = option.Text;
                    option.Click();
                    break;
                }
            }
            return result;

        }

        public void SelectGender(Enums.Gender gender)
        {
            switch (gender)
            {
                case Enums.Gender.Male:
                    webDriverFacade.ClickElement(Elements.maleRadioButton);
                    break;
                case Enums.Gender.Female:
                    webDriverFacade.ClickElement(Elements.femaleRadioButton);
                    break;
                default:
                    throw new Exception($"{gender} is not a part of the list of enums");
            }
        }

        public void EnterAge(int age)
        {
            if (age < MINIMUM_AGE || age > MAXIMUM_AGE)
            {
                throw new Exception("The age provided is out of the vaild range");
            }
            EnterFieldValue(Elements.bmrAgeField, age.ToString());
        }

        public void SelectBmrEstimationFormula(Enums.EstimationFormula estimationFormula, string bodyFatPercentage)
        {
            switch (estimationFormula)
            {
                case Enums.EstimationFormula.MifflinStJeor:
                    webDriverFacade.ClickElement(Elements.bmrFormula1RadioButton);
                    break;
                case Enums.EstimationFormula.RevisedHarrisBenedict:
                    webDriverFacade.ClickElement(Elements.bmrFormula2RadioButton);
                    break;
                case Enums.EstimationFormula.KatchMcArdle:
                    webDriverFacade.ClickElement(Elements.bmrFormula3RadioButton);
                    EnterFieldValue(Elements.bodyFatPercentageField, bodyFatPercentage);
                    break;
                default:
                    throw new Exception($"{estimationFormula} is not a part of the list of enums");
            }
        }

        public void SelectResultsUnit(Enums.ResultsUnit resultUnit)
        {
            switch (resultUnit)
            {
                case Enums.ResultsUnit.Calories:
                    webDriverFacade.ClickElement(Elements.caloriesResultUnit);
                    break;
                case Enums.ResultsUnit.Kilojoules:
                    webDriverFacade.ClickElement(Elements.kilojoulesResultUnit);
                    break;
                default:
                    throw new Exception($"{resultUnit} is not a part of the list of enums");
            }
        }

        public int CalculateBmr(Enums.EstimationFormula formula, int age, Enums.Gender gender, string heightInCm, 
            string weightInKg, string bodyFatPercentage)
        {
            var result = 0.0;

            switch (formula)
            {
                case Enums.EstimationFormula.MifflinStJeor:
                    result = MifflinStJeorFormula(age, gender, heightInCm, weightInKg);
                    break;
                case Enums.EstimationFormula.RevisedHarrisBenedict:
                    result = RevisedHarrisBenedictFormula(age, gender, heightInCm, weightInKg);
                    break;
                default:
                     result = KATMAC_ADD_VALUE + KATMAC_MULTIPLIER * (KATMAC_SUBTRACTOR - (Convert.ToDouble(bodyFatPercentage) / PERCENTAGE)) * Convert.ToDouble(weightInKg);
                    break;
            }

            return Convert.ToInt32(result);
        }

        public dynamic FeetInchesToCmConvertion(dynamic heightInFeet, dynamic heightInInches)
        {
            return (heightInFeet * ONE_FEET_IN_CM) + (heightInInches * ONE_INCH_IN_CM);
        }

        public dynamic PoundToKgConvertion(dynamic weightInPounds)
        {
            return ONE_POUND_IN_KG * weightInPounds;
        }
        
        #endregion

        #region Private methods

        private double RevisedHarrisBenedictFormula(int age, Enums.Gender gender, string heightInCm, string weightInKg)
        {
            double result;
            switch (gender)
            {
                case Enums.Gender.Male:
                    result = HARRISBEN_MALE_WEIGHT_MULTIPLIER * Convert.ToDouble(weightInKg) + HARRISBEN_MALE_HEIGHT_MULTIPLIER * Convert.ToDouble(heightInCm) - HARRISBEN_MALE_AGE_MULTIPLIER * Convert.ToInt32(age) + HARRISBEN_MALE_AGE_ADDER;
                    break;
                default:
                    result = HARRISBEN_FEMALE_WEIGHT_MULTIPLIER * Convert.ToDouble(weightInKg) + HARRISBEN_FEMALE_HEIGHT_MULTIPLIER * Convert.ToDouble(heightInCm) - HARRISBEN_FEMALE_AGE_MULTIPLIER * Convert.ToInt32(age) + HARRISBEN_FEMALE_AGE_ADDER;
                    break;
            }

            return result;
        }

        private double MifflinStJeorFormula(int age, Enums.Gender gender, string heightInCm, string weightInKg)
        {
            double result;
            switch (gender)
            {
                case Enums.Gender.Male:
                    result = MIFFJEOR_WEIGHT_MULTIPLIER * Convert.ToDouble(weightInKg) + MIFFJEOR_HEIGHT_MULTIPLIER * Convert.ToDouble(heightInCm) - MIFFJEOR_AGE_MULTIPLIER * Convert.ToInt32(age) + MIFFJEOR_MALE_AGE_ADDER;
                    break;
                default:
                    result = MIFFJEOR_WEIGHT_MULTIPLIER * Convert.ToDouble(weightInKg) + MIFFJEOR_HEIGHT_MULTIPLIER * Convert.ToDouble(heightInCm) - MIFFJEOR_AGE_MULTIPLIER * Convert.ToInt32(age) - MIFFJEOR_FEMALE_AGE_ADDER;
                    break;
            }

            return result;
        }

        #endregion
    }
}
