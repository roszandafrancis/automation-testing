﻿

namespace Calculator.Net.common
{
    public class Enums
    {
        public enum ResultsUnit
        {
            Calories,
            Kilojoules
        }

        public enum EstimationFormula
        {
            MifflinStJeor,
            RevisedHarrisBenedict,
            KatchMcArdle
        }

        public enum Gender
        {
            Male,
            Female
        }
        
        public enum Temperature
        {
            Celsius,
            Kelvin,
            Fahrenheit
        }
    }
}
