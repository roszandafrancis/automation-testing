﻿using OpenQA.Selenium;
using System.Configuration;

namespace Calculator.Net.pageobjects
{
    /// <summary>
    /// This class holds all the Element Initializations from the app.config file.
    /// The respective classes will call this class to access their elements,
    /// As well as any class that may need access to another class' element
    /// </summary>
    public class Elements
    {

        #region HomePage Elements
        public static string homePage_Url = ConfigurationManager.AppSettings["HomePage_Url"];
        public static By logoButton = By.Id(ConfigurationManager.AppSettings["logo_Id"]);
        public static By salesTaxCalculatorLink = By.LinkText(ConfigurationManager.AppSettings["salesTaxCalculator_LinkText"]);
        public static By amortizationCalculatorLink = By.LinkText(ConfigurationManager.AppSettings["amortizationCalculator_LinkText"]);
        public static By bmrCalculatorLink = By.LinkText(ConfigurationManager.AppSettings["bmrCalculator_LinkText"]);
        public static By percentageCalculatorLink = By.LinkText(ConfigurationManager.AppSettings["percentageCalculator_LinkText"]);
        public static By ageCalculatorLink = By.LinkText(ConfigurationManager.AppSettings["ageCalculator_LinkText"]);
        #endregion

        #region SalesTaxCalculatorPage Elements
        public static string salesTaxCalculatorPage_Url = ConfigurationManager.AppSettings["salesTaxCalculatorPage_Url"];
        public static By beforeTaxField = By.Name(ConfigurationManager.AppSettings["beforeTaxField_Name"]);
        public static By salesTaxRateField = By.Name(ConfigurationManager.AppSettings["salesTaxRateField_Name"]);
        public static By afterTaxPriceField = By.Name(ConfigurationManager.AppSettings["afterTaxPriceField_Name"]);
        public static By salesTaxCalculateButton = By.XPath(ConfigurationManager.AppSettings["calculateButton_Xpath"]);
        public static By salesTaxDefinition = By.XPath(ConfigurationManager.AppSettings["salesTaxDefinition_Xpath"]);
        public static By resultHeading = By.XPath(ConfigurationManager.AppSettings["resultHeading_Xpath"]);
        public static By resultBeforeTaxPrice = By.XPath(ConfigurationManager.AppSettings["resultBeforeTaxPrice_Xpath"]);
        public static By resultSaleTaxRatePercentage = By.XPath(ConfigurationManager.AppSettings["resultSaleTaxRatePercentage_Xpath"]);
        public static By resultSaleTaxRateDollars = By.XPath(ConfigurationManager.AppSettings["resultSaleTaxDollars_Xpath"]);
        public static By resultAfterTaxPrice = By.XPath(ConfigurationManager.AppSettings["resultAfterTaxPrice_Xpath"]);
        public static By allFields_ResultBeforeTaxPrice = By.XPath(ConfigurationManager.AppSettings["allFields_ResultBeforeTaxPrice_Xpath"]);
        public static By allFields_resultSaleTaxRatePercentage = By.XPath(ConfigurationManager.AppSettings["allFields_resultSaleTaxRatePercentage_Xpath"]);
        public static By errorMsg = By.XPath(ConfigurationManager.AppSettings["errorMsgBeforeTaxPrice_Xpath"]);
        public static By errorMsgSalesTaxRate = By.XPath(ConfigurationManager.AppSettings["errorMsgSalesTaxRate_Xpath"]);
        public static By errorMsgAfterTaxPrice = By.XPath(ConfigurationManager.AppSettings["errorMsgAfterTaxPrice_Xpath"]);
        #endregion

        #region BMRCalculatorPage Elements
        public static string bmrCalculatorPage_Url = ConfigurationManager.AppSettings["bmrCalculatorPage_Url"];
        public static By bmrDefinition = By.XPath(ConfigurationManager.AppSettings["bmrDefinition_Xpath"]);
        public static By bmrAgeField = By.Id(ConfigurationManager.AppSettings["ageField_Id"]);
        public static By maleRadioButton = By.Id(ConfigurationManager.AppSettings["maleRadioButton_Id"]);
        public static By femaleRadioButton = By.Id(ConfigurationManager.AppSettings["femaleRadioButton_Id"]);
        public static By bmrCalculateButton = By.XPath(ConfigurationManager.AppSettings["bmrCalculateButton_Xpath"]);
        public static By bmrClearButton = By.ClassName(ConfigurationManager.AppSettings["clearButton_ClassName"]);
        public static By usUnitsTab = By.LinkText(ConfigurationManager.AppSettings["usUnitsTab_LinkText"]);
        public static By heightInFeetField = By.Id(ConfigurationManager.AppSettings["heightInFeetField_Id"]);
        public static By heightInInchesField = By.Id(ConfigurationManager.AppSettings["heightInInchesField_Id"]);
        public static By weightInPoundField = By.Id(ConfigurationManager.AppSettings["weightInPoundField_Id"]);
        public static By metricUnitsTab = By.LinkText(ConfigurationManager.AppSettings["metricUnitsTab_LinkText"]);
        public static By heightInCmField = By.Id(ConfigurationManager.AppSettings["heightInCmField_Id"]);
        public static By weightInKgField = By.Id(ConfigurationManager.AppSettings["weightInKgField_Id"]);
        public static By settingsLink = By.Id(ConfigurationManager.AppSettings["settingsLink_Id"]);
        public static By caloriesResultUnit = By.Id(ConfigurationManager.AppSettings["caloriesResultUnit_Id"]);
        public static By kilojoulesResultUnit = By.Id(ConfigurationManager.AppSettings["kilojoulesResultUnit_Id"]);
        public static By bmrFormula1RadioButton = By.Id(ConfigurationManager.AppSettings["bmrFormula1RadioButton_Id"]);
        public static By bmrFormula2RadioButton = By.Id(ConfigurationManager.AppSettings["bmrFormula2RadioButton_Id"]);
        public static By bmrFormula3RadioButton = By.Id(ConfigurationManager.AppSettings["bmrFormula3RadioButton_Id"]);
        public static By bodyFatPercentageField = By.Name(ConfigurationManager.AppSettings["bodyFatPercentageField_Name"]);
        public static By bmrGeneralResult = By.ClassName(ConfigurationManager.AppSettings["bmrGeneralResult_ClassName"]);
        public static By bmrResultTableHeading = By.ClassName(ConfigurationManager.AppSettings["bmrResultTableHeading_ClassName"]);
        public static By bmrActivityLevelResult_NoExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult1_Xpath"]);
        public static By bmrActivityLevelResult_LittleExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult2_Xpath"]);
        public static By bmrActivityLevelResult_ModerateExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult3_Xpath"]);
        public static By bmrActivityLevelResult_DailyExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult4_Xpath"]);
        public static By bmrActivityLevelResult_IntenseExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult5_Xpath"]);
        public static By bmrActivityLevelResult_VeryIntenseExercise = By.XPath(ConfigurationManager.AppSettings["bmrActivityLevelResult6_Xpath"]);
        public static By bmrOtherUnitsTab = By.LinkText(ConfigurationManager.AppSettings["bmrOtherUnitsTab_LinkText"]);
        public static By bmrOtherUnitsTabIframe = By.Id(ConfigurationManager.AppSettings["bmrOtherUnitsTabIframe_Id"]);
        public static By bmrLengthTab = By.LinkText(ConfigurationManager.AppSettings["bmrLengthTab_LinkText"]);
        public static By bmrTemperatureTab = By.XPath(ConfigurationManager.AppSettings["bmrTemperatureTab_Xpath"]);
        public static By bmrAreaTab = By.LinkText(ConfigurationManager.AppSettings["bmrAreaTab_LinkText"]);
        public static By bmrVolumeTab = By.LinkText(ConfigurationManager.AppSettings["bmrVolumeTab_LinkText"]);
        public static By bmrWeightTab = By.LinkText(ConfigurationManager.AppSettings["bmrWeightTab_LinkText"]);
        public static By bmrFromField = By.Name(ConfigurationManager.AppSettings["bmrFromField_Name"]);
        public static By bmrToField = By.Name(ConfigurationManager.AppSettings["bmrToField_Name"]);
        public static By bmrFromDDL = By.Id(ConfigurationManager.AppSettings["bmrFromDDL_Id"]);
        public static By bmrToDDL = By.Id(ConfigurationManager.AppSettings["bmrToDDL_Id"]);
        #endregion

        #region AmortizationCalculatorPage Elements
        public static string amortizationCalculatorPage_Url = ConfigurationManager.AppSettings["amortizationCalculatorPage_Url"];
        public static By amortizationDefinition = By.XPath(ConfigurationManager.AppSettings["amortizationDefinition_Xpath"]);
        public static By amorLoanAmountField = By.Id(ConfigurationManager.AppSettings["amorLoanAmountField_Id"]);
        public static By amorLoanTermField = By.Id(ConfigurationManager.AppSettings["amorLoanTermField_Id"]);
        public static By amorInterestRateField = By.Id(ConfigurationManager.AppSettings["amorInterestRateField_Id"]);
        public static By amorCalculateButton = By.XPath(ConfigurationManager.AppSettings["amorCalculateButton_Xpath"]);
        public static By amorMonthlyPayResult = By.XPath(ConfigurationManager.AppSettings["amorMonthlyPayResult_Xpath"]);
        public static By amorNumberOfLoanPaymentResult = By.XPath(ConfigurationManager.AppSettings["amorNumberOfLoanPaymentResult_Xpath"]);
        public static By amorTotalLoanPaymentResult = By.XPath(ConfigurationManager.AppSettings["amorTotalLoanPaymentResult_Xpath"]);
        public static By amorTotalInterestResult = By.XPath(ConfigurationManager.AppSettings["amorTotalInterestResult_Xpath"]);
        public static By amorLoanGraph = By.Id(ConfigurationManager.AppSettings["amorLoanGraph_Id"]);
        public static By amorPaymentBreakDownPieChart = By.Id(ConfigurationManager.AppSettings["amorPaymentBreakDownPieChart_Id"]);
        #endregion
    }
}
