﻿using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Support.UI;

namespace Calculator.Net.pageobjects
{
    /// <summary>
    /// Has IWedriver method implementations and is a
    /// Helper class.
    /// </summary>
    public class WebDriverFacade
    {
        /// <summary>C:\Users\roszanda.francis\GitLab\Automation\Calculator.Net\bin\Debug\Calculator.Net.pdb
        /// The web driver instance used for
        /// </summary>
        IWebDriver webDriver;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public WebDriverFacade(IWebDriver webDriver)
        {
            log.Info("Running browser");
            this.webDriver = webDriver;
        }

        #region Most Used Methods
        /// <summary>
        /// Goes to the webpage requested
        /// </summary>
        /// <param name="url">desired website link</param>
        public void NavigateToUrl(string url)
        {
            webDriver.Navigate().GoToUrl(url);
            log.Debug($"successfully navigated to the BMR Calculator page; url = {Elements.bmrCalculatorPage_Url}");
            webDriver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Finds a given element on a webpage and clicks it
        /// </summary>
        /// <param name="element">the element on the webpage to be clicked</param>
        public void ClickElement(By element)
        {
            webDriver.FindElement(element).Click();
        }

        /// <summary>
        /// Finds a given field/element on a webpage and
        /// enters the specified value.
        /// </summary>
        /// <param name="element">the field in which the data is to be typed in</param>
        /// <param name="input">the value to type in the field</param>
        public void TypeInput(By element, string input)
        {
            webDriver.FindElement(element).SendKeys(input);
        }
        
        /// <summary>
        /// Finds a given field/element on a webpage and
        /// removes it's contents
        /// </summary>
        /// <param name="element">the desired field to be cleared<</param>
        public void ClearField(By element)
        {
            webDriver.FindElement(element).Clear();
        }
        #endregion

        #region Get Methods

        /// <summary>
        /// Gets and returns the inner html text of an element
        /// </summary>
        /// <param name="element">the element which the text will be retrieved</param>
        /// <returns>inner html text</returns>
        public string GetElementText(By element)
        {
            return webDriver.FindElement(element).Text;
        }

        /// <summary>
        /// Gets and returns the title of the current page
        /// </summary>
        /// <returns>string title</returns>
        public string GetPageTitle()
        {
            return webDriver.Title;
        }

        /// <summary>
        /// Gets and returns the Url of the current page
        /// </summary>
        /// <returns>string url</returns>
        public string GetPageUrl()
        {
            return webDriver.Url;
        }
        /// <summary>
        /// Finds a given element on a webpage and
        /// returns its longhand CSS property.
        /// </summary>
        /// <param name="element">the element whose CSS value is needed</param>
        /// <param name="cssValue">the longhand CSS property name</param>
        /// <returns>the CSS value of the element</returns>
        public string GetElementCssValue(By element, string cssValue)
        {
            return webDriver.FindElement(element).GetCssValue(cssValue);
        }

        /// <summary>
        /// Finds a given element on a webpage and
        /// returns the value its the specified attribute.
        /// </summary>
        /// <param name="element">the element whose attribute value is needed</param>
        /// <param name="attribute">name of the attribute whose value is needed</param>
        /// <returns>the value its the specified attribute</returns>
        public string GetElementAttribute(By element, string attribute)
        {
            return webDriver.FindElement(element).GetAttribute(attribute);
        }
        #endregion

        #region Utility Methods

        /// <summary>
        /// Performs implicit wait by specified amount of seconds
        /// </summary>
        /// <param name="secondsToWait">The number of seconds to wait</param>
        public void WaitForPageToLoad(int secondsToWait)
        {
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(secondsToWait);
            //IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            //js.ExecuteScript("return window.stop");
        }
        
        /// <summary>
        /// Takes a screenshot and save it to the screenshots file in resources folder
        /// </summary>
        public void TakeScreenshot(string imageName)
        {
            var date = DateTime.Now.ToString().Replace("/", "-").Replace(":", "-");
            var filename = @"C:\Users\roszanda.adm\Pictures\"+ imageName + date + ".png";
            //var filename = ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Calculator.Net" + Path.DirectorySeparatorChar + "resources" + Path.DirectorySeparatorChar + "screenshots" + Path.DirectorySeparatorChar + imageName + date + ".png";

            Screenshot screenshot = ((ITakesScreenshot)webDriver).GetScreenshot();
            screenshot.SaveAsFile(filename, ScreenshotImageFormat.Png);
        }
        
        /// <summary>
        /// Tear down method to close browser
        /// </summary>
        public void CloseBrowser()
        {
            webDriver.Quit();
        }
        #endregion

        #region Least Used Methods

        /// <summary>
        /// Finds a given element and check if it is selected or not.
        /// </summary>
        /// <param name="element">the element to check whether or not is selected</param>
        /// <returns>The boolean value indication whether or not the element is selected</returns>
        public bool IsChecked(By element)
        {
            return webDriver.FindElement(element).Selected;
        }
        
        /// <summary>
        /// Finds a given select element and uses it to initalize the SelectElement class.
        /// </summary>
        /// <param name="element">the element which holds the entire drop down list </param>
        /// <returns>An initalized selectElement</returns>
        public SelectElement InitalizeDDLForSelection(By element)
        {
            var dropDownList = webDriver.FindElement(element);
            return new SelectElement(dropDownList);
        }
        
        /// <summary>
        /// Switches from the current window to the specified iFrame
        /// </summary>
        /// <param name="frameIndex">the zero based index that identifies the frame's location.
        /// E.g: 0 means its the 1st iFrame on the page, 1 = 2nd iFrame etc.</param>
        public void SwitchToIframe(int frameIndex)
        {
            WaitForPageToLoad(5);
            webDriver.SwitchTo().Frame(frameIndex);
        }

        /// <summary>
        /// Finds a given element and checks whether or not it is enabled
        /// </summary>
        /// <param name="element">the element to be check if enabled or not</param>
        /// <returns>The boolean answer whether or not the field is enabled</returns>
        public bool IsElementEnabled(By element)
        {
            return webDriver.FindElement(element).Enabled;
        }
        #endregion
        
        public bool ElementIsDisplayed(By element)
        {
            return webDriver.FindElement(element).Displayed;
        }
    }
}
