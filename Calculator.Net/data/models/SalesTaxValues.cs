﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Calculator.Net.data.models
{
    public class SalesTaxValues
    {
        public string BeforeTaxPrice { get; set; }
        public string SalesTaxRate { get; set; }
        public string AfterTaxPrice { get; set; }
            

        public static IEnumerable<SalesTaxValues> ReadValuesFromFile()
        {
            var filePath = ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "Calculator.Net" + Path.DirectorySeparatorChar + "data" + Path.DirectorySeparatorChar + "jsonfiles" + Path.DirectorySeparatorChar + "SalesTaxCalculator.json";
            var jsonString = File.ReadAllText(filePath);
            var salesTaxValues = JsonConvert.DeserializeObject<IEnumerable<SalesTaxValues>>(jsonString);

            return salesTaxValues;
        }

    }
}
