﻿using Calculator.Net.pageobjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System;

namespace Calculator.Net.pageobjectTests
{
    [TestClass]
    public class SalesTaxCalculatorPageTest
    {
        #region Declarations
        WebDriverFacade webDriverFacade;
        SalesTaxCalculatorPage salesTaxCalculatorPage;

        static string expectedErrorMsgColour = "rgba(255, 0, 0, 1)";
        static string expectedBeforeTaxPriceErrorMsg = "Please provide a valid before tax price.";
        static string expectedSalesTaxRateErrorMsg = "Please provide a valid sales tax rate.";
        static string expectedAfterTaxPriceErrorMsg = "Please provide a valid after tax price.";

        #endregion

        [TestInitialize]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            webDriverFacade = new WebDriverFacade(new ChromeDriver(chromeOptions));
            //webDriverFacade = new WebDriverFacade(new ChromeDriver());

            webDriverFacade.NavigateToUrl(Elements.salesTaxCalculatorPage_Url);
            salesTaxCalculatorPage = new SalesTaxCalculatorPage(webDriverFacade);
        }


        [TestMethod]
        public void Verify_DefaultFieldValuesArePresent()
        {
            //Arrange
            var expectedBeforeTaxPrice = "100";
            var expectedSalesTaxRate = "6.5";
            var expectedAfterTaxPrice = ""; //empty
            var attribute = "value";

            //Act
            var actualBeforeTaxPrice = webDriverFacade.GetElementAttribute(Elements.beforeTaxField, attribute);
            var actualSalesTaxRate = webDriverFacade.GetElementAttribute(Elements.salesTaxRateField, attribute);
            var actualAfterTaxPrice = webDriverFacade.GetElementAttribute(Elements.afterTaxPriceField, attribute);

            //Assert
            Assert.AreEqual(expectedBeforeTaxPrice, actualBeforeTaxPrice);
            Assert.AreEqual(expectedSalesTaxRate, actualSalesTaxRate);
            Assert.AreEqual(expectedAfterTaxPrice, actualAfterTaxPrice);
        }
        
        [TestMethod]
        public void Verify_ResultHeading()
        {
            //Arrange
            var filename = "result-heading-";
            var expectedResultHeading = "Result";
            var expectedResultBkgrdColour = "rgba(81, 132, 40, 1)"; //Did this because the webdriver function will ONLY return the rgba colour

            //Act
            salesTaxCalculatorPage.ClickCalculate();
            var actualResultHeading = webDriverFacade.GetElementText(Elements.resultHeading);
            var actualResultBkgrdColour = webDriverFacade.GetElementCssValue(Elements.resultHeading, "background-color");
            webDriverFacade.TakeScreenshot(filename);

            //Assert
            Assert.AreEqual(expectedResultHeading, actualResultHeading);
            Assert.AreEqual(expectedResultBkgrdColour, actualResultBkgrdColour);
        }

        [DataTestMethod]
        [DataRow(3009, 19)]
        [DataRow(5854, 768.65)]
        [DataRow(0.09, 0.03)]
        //[DataRow(986665764323, 9087654323333333)]
        public void UserCan_CalculateAfterTaxPrice(dynamic beforeTaxPrice, dynamic salesTaxRate)
        {
            //Arrange

            //Act
            salesTaxCalculatorPage.CalculateAfterTaxPrice(beforeTaxPrice.ToString(), salesTaxRate.ToString());
            var actualAfterTaxPriceResult = webDriverFacade.GetElementText(Elements.resultAfterTaxPrice);
            var saleTax = beforeTaxPrice * (salesTaxRate / 100.00);
            var expectedAfterTaxPriceResult = Math.Round(beforeTaxPrice + saleTax, 2); 
            
            //Assert
            Assert.AreEqual("$"+expectedAfterTaxPriceResult.ToString(), actualAfterTaxPriceResult.Replace(",","")); //the replace was added because the website returns the number like this: 12,000 and my calculation returns 12000
        
        }
        

        [DataTestMethod]
        [DataRow("43d", "5.97")]
        [DataRow("0", "0")]
        public void UserCannot_CalculateAfterTaxPriceAndPresentedWithBeforeTaxPriceErrorMessage(string beforeTaxPrice, string salesTaxRate)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateAfterTaxPrice(beforeTaxPrice, salesTaxRate);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedBeforeTaxPriceErrorMsg);
        }
        
        [DataTestMethod]
        [DataRow("20886", "-435")]
        public void UserCannot_CalculateAfterTaxPriceAndPresentedWithSalesTaxRateErrorMessage(string beforeTaxPrice, string salesTaxRate)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateAfterTaxPrice(beforeTaxPrice, salesTaxRate);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedSalesTaxRateErrorMsg);
        }

        [DataTestMethod]
        [DataRow(5854, 778468.65)]
        [DataRow(3009, 19)]
        [DataRow(0.03, 0.09)]
        [DataRow(0, 0)]
        public void UserCan_CalculateBeforeTaxPrice(dynamic salesTaxRate, dynamic afterTaxPrice)
        {
            //Arrange

            //Act
            salesTaxCalculatorPage.CalculateBeforeTaxPrice(salesTaxRate.ToString(), afterTaxPrice.ToString());
            var actualBeforeTaxPriceResult = webDriverFacade.GetElementText(Elements.resultBeforeTaxPrice);
            var saleTax = afterTaxPrice - (afterTaxPrice / ((100.00 + salesTaxRate) / 100.00));
            var expectedBeforeTaxPriceResult = afterTaxPrice - saleTax;

            //Assert
            Assert.AreEqual("$" + expectedBeforeTaxPriceResult.ToString("0.00"), actualBeforeTaxPriceResult.Replace(",","")); //the replace was added because the website returns the number like this: 12,000 and my calculation returns 12000
        }

        [DataTestMethod]
        [DataRow("43d", "5.97")]
        public void UserCannot_CalculateBeforeTaxPriceAndPresentedWithSalesTaxRateErrorMessage(string salesTaxRate, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateBeforeTaxPrice(salesTaxRate, afterTaxPrice);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedSalesTaxRateErrorMsg);
        }
        
        [DataTestMethod]
        [DataRow("20886.8", "-435")]
        public void UserCannot_CalculateBeforeTaxPriceAndPresentedWithAfterTaxPriceErrorMessage(string salesTaxRate, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateBeforeTaxPrice(salesTaxRate, afterTaxPrice);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedAfterTaxPriceErrorMsg);
        }

        [DataTestMethod]
        [DataRow(78.9, 9234.56)]
        [DataRow(1, 9)]
        public void UserCan_CalculateSalesTaxRate(dynamic beforeTaxPrice, dynamic afterTaxPrice)
        {
            //Arrange

            //Act
            salesTaxCalculatorPage.CalculateSalesTaxRate(beforeTaxPrice.ToString(), afterTaxPrice.ToString());
            var actualSalesTaxRateResult_InDollars = webDriverFacade.GetElementText(Elements.resultSaleTaxRateDollars);
            var actualSalesTaxRateResult_InPercentage = webDriverFacade.GetElementText(Elements.resultSaleTaxRatePercentage);
            var expectedSalesTaxRateResult_InDollars = afterTaxPrice - beforeTaxPrice;
            var expectedSalesTaxRateResult_InPercentage = (expectedSalesTaxRateResult_InDollars / beforeTaxPrice) * 100.00;

            //Assert
            Assert.AreEqual("$" + expectedSalesTaxRateResult_InDollars.ToString("0.00"), actualSalesTaxRateResult_InDollars.Replace(",","")); //the replace was added because the website returns the number like this: 12,000 and my calculation returns 12000
            Assert.AreEqual(expectedSalesTaxRateResult_InPercentage.ToString("0.00") + "%", actualSalesTaxRateResult_InPercentage.Replace(",", "")); 
        }

        [DataTestMethod]
        [DataRow("622", "85")]
        public void UserCannot_CalculateSalesTaxRateAndPresentedWithAfterTaxPriceAmountErrorMessage(string beforeTaxPrice, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";
            var expectedAfterTaxPriceAmountErrorMsg = "After tax price can not be smaller than before tax price.";

            //Act
            salesTaxCalculatorPage.CalculateSalesTaxRate(beforeTaxPrice, afterTaxPrice);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedAfterTaxPriceAmountErrorMsg);
        }
        
        [DataTestMethod]
        [DataRow("43d", "5.97")]
        [DataRow("0", "0")]
        public void UserCannot_CalculateSalesTaxRateAndPresentedWithBeforeTaxPriceErrorMessage(string beforeTaxPrice, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateSalesTaxRate(beforeTaxPrice, afterTaxPrice);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedBeforeTaxPriceErrorMsg);
        }
        
        [DataTestMethod]
        [DataRow("20886.8", "-435")]
        public void UserCannot_CalculateSalesTaxRateAndPresentedWithAfterTaxPriceErrorMessage(string beforeTaxPrice, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";

            //Act
            salesTaxCalculatorPage.CalculateSalesTaxRate(beforeTaxPrice, afterTaxPrice);
            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.AreEqual(actualErrorMsg, expectedAfterTaxPriceErrorMsg);
        }

        [DataTestMethod]
        [DataRow("78.90", "90", "9234.56")]
        public void EnteringValuesInAllFields_ShouldCalcuateAfterTaxPriceOnly(string beforeTaxPrice, string salesTaxRate, string afterTaxPrice)
        {
            //Arrange
            var expectedAfterTaxPrice = "$149.91";
            var salesTaxRate_InDollars = "$71.01";

            //Act
            salesTaxCalculatorPage.EnterFieldValue(Elements.beforeTaxField, beforeTaxPrice);
            salesTaxCalculatorPage.EnterFieldValue(Elements.salesTaxRateField, salesTaxRate);
            salesTaxCalculatorPage.EnterFieldValue(Elements.afterTaxPriceField, afterTaxPrice);
            salesTaxCalculatorPage.ClickCalculate();

            var actualBeforeTaxPriceResult = webDriverFacade.GetElementText(Elements.allFields_ResultBeforeTaxPrice);
            var actualSalesTaxRateResult = webDriverFacade.GetElementText(Elements.allFields_resultSaleTaxRatePercentage);
            var actualAfterTaxPriceResult = webDriverFacade.GetElementText(Elements.resultAfterTaxPrice);

            //Assert
            Assert.IsTrue(actualBeforeTaxPriceResult.Contains("$" + beforeTaxPrice));
            Assert.IsTrue(actualSalesTaxRateResult.Contains(salesTaxRate));
            Assert.IsTrue(actualSalesTaxRateResult.Contains(salesTaxRate_InDollars));
            Assert.AreEqual(expectedAfterTaxPrice, actualAfterTaxPriceResult);
        }
        
        [DataTestMethod]
        [DataRow("", "", "")]
        [DataRow("78.90", "", "")]
        [DataRow("", "78.90", "")]
        [DataRow("", "", "78.90")]
        public void UserMust_PopulateAtLeastTwolFieldsWithValidNumbersToDoCalculations(string beforeTaxPrice, string salesTaxRate, string afterTaxPrice)
        {
            //Arrange
            var cssValue = "color";
            var expectedErrorMsgColour = "rgba(255, 0, 0, 1)";
            var expectedErrorMsg = "Please provide at least two values to calculate.";


            //Act
            salesTaxCalculatorPage.EnterFieldValue(Elements.beforeTaxField, beforeTaxPrice);
            salesTaxCalculatorPage.EnterFieldValue(Elements.salesTaxRateField, salesTaxRate);
            salesTaxCalculatorPage.EnterFieldValue(Elements.afterTaxPriceField, afterTaxPrice);
            salesTaxCalculatorPage.ClickCalculate();

            var actualErrorMsg = webDriverFacade.GetElementText(Elements.errorMsg);
            var actualErrorMsgColour = webDriverFacade.GetElementCssValue(Elements.errorMsg, cssValue);

            //Assert
            Assert.AreEqual(expectedErrorMsgColour, actualErrorMsgColour);
            Assert.IsTrue(actualErrorMsg.Equals(expectedErrorMsg));
        }

        [TestCleanup]
        public void TearDown()
        {
           webDriverFacade.CloseBrowser();
        }
    }
}
