﻿using Calculator.Net.pageobjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;

namespace Calculator.Net.pageobjectTests
{
    [TestClass]
    public class HomePageTests
    {
        #region Declarations
        WebDriverFacade webDriverFacade;
        HomePage home;
        #endregion

        [TestInitialize]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            webDriverFacade = new WebDriverFacade(new ChromeDriver(chromeOptions));

            //webDriverFacade = new WebDriverFacade(new ChromeDriver());
            webDriverFacade.NavigateToUrl(Elements.homePage_Url);
            home = new HomePage(webDriverFacade);
        }

        [TestMethod]
        public void UserCan_NavigateToSalesTaxCalculatorPage()
        {
            //Arrange
            var expectedPageTitle = "Sales Tax Calculator";
            var expectedSalesTaxDefinition = "A sales tax is a consumption tax paid to a government on the sale of certain goods and services.";
            var expectedUrl = Elements.salesTaxCalculatorPage_Url;

            //Act
            home.GoToSalesTaxCalculatorPage();
            var actualPageTitle = webDriverFacade.GetPageTitle();
            var actualSalesTaxDefinition = webDriverFacade.GetElementText(Elements.salesTaxDefinition);
            var actualUrl = webDriverFacade.GetPageUrl();

            //Assert
            Assert.AreEqual(expectedUrl, actualUrl);
            Assert.AreEqual(expectedPageTitle, actualPageTitle);
            Assert.IsTrue(actualSalesTaxDefinition.Contains(expectedSalesTaxDefinition));
        }

        [TestMethod]
        public void UserCan_NavigateToBmrCalculatorPage()
        {
            //Arrange
            var expectedPageTitle = "BMR Calculator";
            var expectedBmrDefinition = "The basal metabolic rate (BMR) is the amount of energy needed while resting in a temperate environment when the digestive system is inactive";
            var expectedUrl = Elements.bmrCalculatorPage_Url;

            //Act
            home.GoToBmrCalculatorPage();
            var actualPageTitle = webDriverFacade.GetPageTitle();
            var actualBmrDefinition = webDriverFacade.GetElementText(Elements.bmrDefinition);
            var actualUrl = webDriverFacade.GetPageUrl();

            //Assert
            Assert.AreEqual(expectedUrl, actualUrl);
            Assert.AreEqual(expectedPageTitle, actualPageTitle);
            Assert.IsTrue(actualBmrDefinition.Contains(expectedBmrDefinition));
        }
        
        [TestMethod]
        public void UserCan_NavigateToAmortizationCalculatorPage()
        {
            //Arrange
            var expectedPageTitle = "Amortization Calculator";
            var expectedAmortizationDefinition = "There are two general definitions of amortization. The first is the systematic repayment of a loan over time.";
            var expectedUrl = Elements.amortizationCalculatorPage_Url;

            //Act
            home.GoToAmoritizationCalculatorPage();
            var actualPageTitle = webDriverFacade.GetPageTitle();
            var actualamortizationDefinition = webDriverFacade.GetElementText(Elements.amortizationDefinition);
            var actualUrl = webDriverFacade.GetPageUrl();

            //Assert
            Assert.AreEqual(expectedUrl, actualUrl);
            Assert.AreEqual(expectedPageTitle, actualPageTitle);
            Assert.IsTrue(actualamortizationDefinition.Contains(expectedAmortizationDefinition));
        }

        [TestCleanup]
        public void TearDown()
        {
            webDriverFacade.CloseBrowser();
        }
    }
}
