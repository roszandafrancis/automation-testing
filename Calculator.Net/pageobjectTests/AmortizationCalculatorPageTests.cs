﻿using Calculator.Net.pageobjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;

namespace Calculator.Net.pageobjectTests
{
    [TestClass]
    public class AmortizationCalculatorPageTests
    {
        #region Declarations
        WebDriverFacade webDriverFacade;
        AmortizationCalculatorPage amortizationCalculatorPage;
        #endregion

        [TestInitialize]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            webDriverFacade = new WebDriverFacade(new ChromeDriver(chromeOptions));
            //webDriverFacade = new WebDriverFacade(new ChromeDriver());

            webDriverFacade.NavigateToUrl(Elements.amortizationCalculatorPage_Url);
            amortizationCalculatorPage = new AmortizationCalculatorPage(webDriverFacade);
        }

        [DataTestMethod]
        [DataRow("500000", "15", "6", "4,219.28", "180", "$759,471.15", "$259,471.15")]
        [DataRow("2000000", "4", "15", "55,661.50", "48", "$2,671,751.83", "$671,751.83")]
        public void UserCan_CalculateLoanAmortization(string loanAmount, string loanTerm, string interestRate, string monthlyPayResult,
             string numberofLoanPayment, string expectedLoanPaymentAmountResult, string expectedTotalInterestResult)
        {
            //Arrange
            var expectedMonthlyPayResult = $"Monthly Pay:   ${monthlyPayResult}";
            var expectedNumberofLoanPaymentResult = $"Total of {numberofLoanPayment} Loan Payments";

            //Act
            amortizationCalculatorPage.CalculateLoanAmortization(loanAmount, loanTerm, interestRate);
            var actualMonthlyPayResult = webDriverFacade.GetElementText(Elements.amorMonthlyPayResult);
            var actualNumberofLoanPaymentResult = webDriverFacade.GetElementText(Elements.amorNumberOfLoanPaymentResult);
            var actualLoanPaymentAmountResult = webDriverFacade.GetElementText(Elements.amorTotalLoanPaymentResult);
            var actualTotalInterestResult = webDriverFacade.GetElementText(Elements.amorTotalInterestResult);

            //Assert
            Assert.AreEqual(expectedMonthlyPayResult, actualMonthlyPayResult);
            Assert.AreEqual(expectedNumberofLoanPaymentResult, actualNumberofLoanPaymentResult);
            Assert.AreEqual(expectedLoanPaymentAmountResult, actualLoanPaymentAmountResult);
            Assert.AreEqual(expectedTotalInterestResult, actualTotalInterestResult);
        }

        [TestMethod]
        public void UserCan_SeeLoanAmortizationGraph()
        {
            //Arrange

            //Act
            var actualLoanAmortizationGraph = webDriverFacade.ElementIsDisplayed(Elements.amorLoanGraph);

            //Assert
            Assert.IsTrue(actualLoanAmortizationGraph);
        }
        
        [TestMethod]
        public void UserCan_SeePaymentBreakdownPieChart()
        {
            //Arrange

            //Act
            var actualPaymentBreakdownPieChart = webDriverFacade.ElementIsDisplayed(Elements.amorPaymentBreakDownPieChart);

            //Assert
            Assert.IsTrue(actualPaymentBreakdownPieChart);
        }
    }
}
