﻿using Calculator.Net.common;
using Calculator.Net.pageobjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using System;

namespace Calculator.Net.pageobjectTests
{
    [TestClass]
    public class BMRCalculatorPageTests
    {
        #region Declarations
        WebDriverFacade webDriverFacade;
        BMRCalculatorPage bmrCalculatorPage;
        #endregion

        [TestInitialize]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            webDriverFacade = new WebDriverFacade(new ChromeDriver(chromeOptions));
            //webDriverFacade = new WebDriverFacade(new ChromeDriver());

            webDriverFacade.NavigateToUrl(Elements.bmrCalculatorPage_Url);
            bmrCalculatorPage = new BMRCalculatorPage(webDriverFacade);
        }

        [TestMethod]
        public void MaleUserCan_CalculateBmrUsingDefaultSettings_InMetricUnits()
        {
            //Arrange
            #region Expected Values
            int age = 38;
            var gender = Enums.Gender.Male;
            var heightInCm = "179.8";
            var weightInKg = "141.521";
            var expectedResultMale = "2,354";

            var expectedResultTableHeading = "Calorie";
            var expectedBmrGeneralResultMale = $"BMR = {expectedResultMale} Calories/day"; 
            var expectedMaleActivityLevelNoExercise = "2,825"; 
            var expectedMaleActivityLevelLittleExercise = "3,237"; 
            var expectedMaleActivityLevelModerateExercise = "3,449"; 
            var expectedMaleActivityLevelDailyExercise = "3,649"; 
            var expectedMaleActivityLevelIntenseExercise = "4,061"; 
            var expectedMaleActivityLevelVeryIntenseExercise = "4,473";
            #endregion

            //Act
            bmrCalculatorPage.CalculateBmrInMetricUnits(age, gender, heightInCm, weightInKg);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var actualResultTableHeading = webDriverFacade.GetElementText(Elements.bmrResultTableHeading);
            var actualActivityLevelNoExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_NoExercise);
            var actualActivityLevelLittleExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_LittleExercise);
            var actualActivityLevelModerateExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_ModerateExercise);
            var actualActivityLevelDailyExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_DailyExercise);
            var actualActivityLevelIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_IntenseExercise);
            var actualActivityLevelVeryIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_VeryIntenseExercise);

            //Assert
            Assert.AreEqual(actualBmrGeneralResult, expectedBmrGeneralResultMale);
            Assert.AreEqual(expectedResultTableHeading, actualResultTableHeading);
            Assert.AreEqual(actualActivityLevelNoExercise, expectedMaleActivityLevelNoExercise);
            Assert.AreEqual(actualActivityLevelLittleExercise, expectedMaleActivityLevelLittleExercise);
            Assert.AreEqual(actualActivityLevelModerateExercise, expectedMaleActivityLevelModerateExercise);
            Assert.AreEqual(actualActivityLevelDailyExercise, expectedMaleActivityLevelDailyExercise);
            Assert.AreEqual(actualActivityLevelIntenseExercise, expectedMaleActivityLevelIntenseExercise);
            Assert.AreEqual(actualActivityLevelVeryIntenseExercise, expectedMaleActivityLevelVeryIntenseExercise);
        }
        
        [TestMethod]
        public void FemaleUserCan_CalculateBmrUsingDefaultSettings_InMetricUnits()
        {
            //Arrange
            #region Expected Values
            int age = 72;
            var gender = Enums.Gender.Female;
            var heightInCm = "131";
            var weightInKg = "51.1";
            var expectedResultFemale = "809";

            var expectedResultTableHeading = "Calorie";
            var expectedBmrGeneralResultFemale = $"BMR = {expectedResultFemale} Calories/day";
            var expectedFemaleActivityLevel_NoExercise = "971"; 
            var expectedFemaleActivityLevel_LittleExercise = "1,112"; 
            var expectedFemaleActivityLevel_ModerateExercise = "1,185"; 
            var expectedFemaleActivityLevel_DailyExercise = "1,254"; 
            var expectedFemaleActivityLevel_IntenseExercise = "1,395"; 
            var expectedFemaleActivityLevel_VeryIntenseExercise = "1,537"; 
            #endregion

            //Act
            bmrCalculatorPage.CalculateBmrInMetricUnits(age, gender, heightInCm, weightInKg);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var actualResultTableHeading = webDriverFacade.GetElementText(Elements.bmrResultTableHeading);
            var actualActivityLevelNoExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_NoExercise);
            var actualActivityLevelLittleExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_LittleExercise);
            var actualActivityLevelModerateExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_ModerateExercise);
            var actualActivityLevelDailyExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_DailyExercise);
            var actualActivityLevelIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_IntenseExercise);
            var actualActivityLevelVeryIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_VeryIntenseExercise);

            //Assert
            Assert.AreEqual(actualBmrGeneralResult, expectedBmrGeneralResultFemale);
            Assert.AreEqual(expectedResultTableHeading, actualResultTableHeading);
            Assert.AreEqual(actualActivityLevelNoExercise, expectedFemaleActivityLevel_NoExercise);
            Assert.AreEqual(actualActivityLevelLittleExercise, expectedFemaleActivityLevel_LittleExercise);
            Assert.AreEqual(actualActivityLevelModerateExercise, expectedFemaleActivityLevel_ModerateExercise);
            Assert.AreEqual(actualActivityLevelDailyExercise, expectedFemaleActivityLevel_DailyExercise);
            Assert.AreEqual(actualActivityLevelIntenseExercise, expectedFemaleActivityLevel_IntenseExercise);
            Assert.AreEqual(actualActivityLevelVeryIntenseExercise, expectedFemaleActivityLevel_VeryIntenseExercise);
        }
        
        [TestMethod]
        public void FemaleUserCan_CalculateBmrUsingDefaultSettings_InUsUnits()
        {
            //Arrange
            #region Expected Values
            int age = 72;
            var gender = Enums.Gender.Female;
            var heightInFeet = "4";
            var heightInInches = "3";
            var weightInPounds = "112.78";
            var expectedResultFemale = "800";

            var expectedResultTableHeading = "Calorie";
            var expectedBmrGeneralResultFemale = $"BMR = {expectedResultFemale} Calories/day";
            var expectedFemaleActivityLevel_NoExercise = "960"; 
            var expectedFemaleActivityLevel_LittleExercise = "1,100"; 
            var expectedFemaleActivityLevel_ModerateExercise = "1,172"; 
            var expectedFemaleActivityLevel_DailyExercise = "1,240"; 
            var expectedFemaleActivityLevel_IntenseExercise = "1,380"; 
            var expectedFemaleActivityLevel_VeryIntenseExercise = "1,520"; 
            #endregion

            //Act
            bmrCalculatorPage.CalculateBmrInUsUnits(age, gender, heightInFeet, heightInInches, weightInPounds);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var actualResultTableHeading = webDriverFacade.GetElementText(Elements.bmrResultTableHeading);
            var actualActivityLevelNoExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_NoExercise);
            var actualActivityLevelLittleExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_LittleExercise);
            var actualActivityLevelModerateExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_ModerateExercise);
            var actualActivityLevelDailyExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_DailyExercise);
            var actualActivityLevelIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_IntenseExercise);
            var actualActivityLevelVeryIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_VeryIntenseExercise);

            //Assert
            Assert.AreEqual(actualBmrGeneralResult, expectedBmrGeneralResultFemale);
            Assert.AreEqual(expectedResultTableHeading, actualResultTableHeading);
            Assert.AreEqual(actualActivityLevelNoExercise, expectedFemaleActivityLevel_NoExercise);
            Assert.AreEqual(actualActivityLevelLittleExercise, expectedFemaleActivityLevel_LittleExercise);
            Assert.AreEqual(actualActivityLevelModerateExercise, expectedFemaleActivityLevel_ModerateExercise);
            Assert.AreEqual(actualActivityLevelDailyExercise, expectedFemaleActivityLevel_DailyExercise);
            Assert.AreEqual(actualActivityLevelIntenseExercise, expectedFemaleActivityLevel_IntenseExercise);
            Assert.AreEqual(actualActivityLevelVeryIntenseExercise, expectedFemaleActivityLevel_VeryIntenseExercise);
        }
        
        [TestMethod]
        public void MaleUserCan_CalculateBmrUsingDefaultSettings_InUsUnits()
        {
            //Arrange
            #region Expected Values
            var age = 38;
            var gender = Enums.Gender.Male;
            var heightInFeet= "5";
            var heightInInches = "9";
            var weightInPounds = "312";
            var expectedResultMale = "2,326";

            var expectedResultTableHeading = "Calorie";
            var expectedBmrGeneralResultMale = $"BMR = {expectedResultMale} Calories/day"; 
            var expectedMaleActivityLevelNoExercise = "2,791"; 
            var expectedMaleActivityLevelLittleExercise = "3,198"; 
            var expectedMaleActivityLevelModerateExercise = "3,407"; 
            var expectedMaleActivityLevelDailyExercise = "3,605"; 
            var expectedMaleActivityLevelIntenseExercise = "4,012"; 
            var expectedMaleActivityLevelVeryIntenseExercise = "4,419";
            #endregion

            //Act
            bmrCalculatorPage.CalculateBmrInUsUnits(age, gender, heightInFeet, heightInInches, weightInPounds);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var actualResultTableHeading = webDriverFacade.GetElementText(Elements.bmrResultTableHeading);
            var actualActivityLevelNoExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_NoExercise);
            var actualActivityLevelLittleExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_LittleExercise);
            var actualActivityLevelModerateExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_ModerateExercise);
            var actualActivityLevelDailyExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_DailyExercise);
            var actualActivityLevelIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_IntenseExercise);
            var actualActivityLevelVeryIntenseExercise = webDriverFacade.GetElementText(Elements.bmrActivityLevelResult_VeryIntenseExercise);

            //Assert
            Assert.AreEqual(actualBmrGeneralResult, expectedBmrGeneralResultMale);
            Assert.AreEqual(expectedResultTableHeading, actualResultTableHeading);
            Assert.AreEqual(actualActivityLevelNoExercise, expectedMaleActivityLevelNoExercise);
            Assert.AreEqual(actualActivityLevelLittleExercise, expectedMaleActivityLevelLittleExercise);
            Assert.AreEqual(actualActivityLevelModerateExercise, expectedMaleActivityLevelModerateExercise);
            Assert.AreEqual(actualActivityLevelDailyExercise, expectedMaleActivityLevelDailyExercise);
            Assert.AreEqual(actualActivityLevelIntenseExercise, expectedMaleActivityLevelIntenseExercise);
            Assert.AreEqual(actualActivityLevelVeryIntenseExercise, expectedMaleActivityLevelVeryIntenseExercise);
        }
        
        [TestMethod]
        public void UserCan_SeeThatSettingsLabelTextIsChangedOnClick()
        {
            //Arrange
            var expectedsettingsLabelOnClickToExpand = "- Settings";
            var expectedsettingsLabelOnClickToContract = "+ Settings";

            //Act
            webDriverFacade.ClickElement(Elements.settingsLink);
            var settingsLabelOnClickToExpand = webDriverFacade.GetElementText(Elements.settingsLink);
            webDriverFacade.ClickElement(Elements.settingsLink);
            var settingsLabelOnClickToContract = webDriverFacade.GetElementText(Elements.settingsLink);

            //Assert
            Assert.AreEqual(expectedsettingsLabelOnClickToExpand, settingsLabelOnClickToExpand);
            Assert.AreEqual(expectedsettingsLabelOnClickToContract, settingsLabelOnClickToContract);
        }
        
        [TestMethod]
        public void UserCan_ClearAllFieldsWithTheClearButton_UsUnitsTab()
        {
            //Arrange

            //Act
            webDriverFacade.ClickElement(Elements.usUnitsTab);
            webDriverFacade.ClickElement(Elements.bmrClearButton);
            var actualBmrAgeField = webDriverFacade.GetElementText(Elements.bmrAgeField);
            var actualMaleRadioButton = webDriverFacade.IsChecked(Elements.maleRadioButton);
            var actualHeightInFeetField = webDriverFacade.GetElementText(Elements.heightInFeetField);
            var actualHeightInInchesField = webDriverFacade.GetElementText(Elements.heightInInchesField);
            var actualWeightInPoundField = webDriverFacade.GetElementText(Elements.weightInPoundField);

            //Assert
            Assert.AreEqual("", actualBmrAgeField);
            Assert.IsTrue(actualMaleRadioButton);
            Assert.AreEqual("", actualHeightInFeetField);
            Assert.AreEqual("", actualHeightInInchesField);
            Assert.AreEqual("", actualWeightInPoundField);
        }
        
        [TestMethod]
        public void UserCan_ClearAllFieldsWithTheClearButton_MetricUnitsTab()
        {
            //Arrange

            //Act
            webDriverFacade.ClickElement(Elements.bmrClearButton);
            var actualBmrAgeField = webDriverFacade.GetElementText(Elements.bmrAgeField);
            var actualMaleRadioButton = webDriverFacade.IsChecked(Elements.maleRadioButton);
            var actualHeightInCmField = webDriverFacade.GetElementText(Elements.heightInCmField);
            var actualWeightInKgField = webDriverFacade.GetElementText(Elements.weightInKgField);

            //Assert
            Assert.AreEqual("", actualBmrAgeField);
            Assert.IsTrue(actualMaleRadioButton);
            Assert.AreEqual("", actualHeightInCmField);
            Assert.AreEqual("", actualWeightInKgField);
        }

        [DataTestMethod]
        //****Haven't figured out the formula for kilojoules yet******
        //[DataRow(72, Enums.Gender.Female, "4", "3", "112.78", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.MifflinStJeor, "")]
        //[DataRow(72, Enums.Gender.Female, "4", "3", "112.78", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        //[DataRow(27, Enums.Gender.Female, "5", "6.5", "170", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.KatchMcArdle, "29")]
        [DataRow(16, Enums.Gender.Male, "6", "0", "125", Enums.ResultsUnit.Calories, Enums.EstimationFormula.KatchMcArdle, "5")]
        [DataRow(27, Enums.Gender.Female, "5", "6.5", "190", Enums.ResultsUnit.Calories, Enums.EstimationFormula.KatchMcArdle, "72")]
        [DataRow(46, Enums.Gender.Male, "6", "1.5", "250", Enums.ResultsUnit.Calories, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        [DataRow(72, Enums.Gender.Female, "4", "3", "112.78", Enums.ResultsUnit.Calories, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        public void UserCan_CalculateBmrUsingDesiredSettings_InUsUnits(int age, Enums.Gender gender, string heightInFeet, string heightInInches, string weightInPounds, 
            Enums.ResultsUnit resultUnit, Enums.EstimationFormula estimationFormula, string bodyFatPercentage)
        {
            //Arrange
            var weightInKg = bmrCalculatorPage.PoundToKgConvertion(Convert.ToDouble(weightInPounds));
            var heightInCm = bmrCalculatorPage.FeetInchesToCmConvertion(Convert.ToDouble(heightInFeet), Convert.ToDouble(heightInInches));

            //Act
            webDriverFacade.ClickElement(Elements.usUnitsTab);
            bmrCalculatorPage.EnterAge(age);
            bmrCalculatorPage.SelectGender(gender);
            bmrCalculatorPage.EnterFieldValue(Elements.heightInFeetField, heightInFeet);
            bmrCalculatorPage.EnterFieldValue(Elements.heightInInchesField, heightInInches);
            bmrCalculatorPage.EnterFieldValue(Elements.weightInPoundField, weightInPounds);

            bmrCalculatorPage.AdjustAllSettings(resultUnit, estimationFormula, bodyFatPercentage);
            var expectedBmrGeneralResult = bmrCalculatorPage.CalculateBmr(estimationFormula, age, gender, heightInCm.ToString(), weightInKg.ToString(), bodyFatPercentage);

            webDriverFacade.ClickElement(Elements.bmrCalculateButton);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var expectedBmrGeneralResultFinal = $"BMR = {expectedBmrGeneralResult} Calories/day";
           
            //Assert
            Assert.AreEqual(expectedBmrGeneralResultFinal, actualBmrGeneralResult.Replace("," , ""));
        }
        

        [DataTestMethod]
        //****Haven't figured out the formula for kilojoules yet******
        //[DataRow(72, Enums.Gender.Female, "4", "3", "112.78", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.MifflinStJeor, "")]
        //[DataRow(72, Enums.Gender.Female, "4", "3", "112.78", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        //[DataRow(27, Enums.Gender.Female, "5", "6.5", "170", Enums.ResultsUnit.Kilojoules, Enums.EstimationFormula.KatchMcArdle, "29")]
        [DataRow(38, Enums.Gender.Male, "179.8", "141.521", Enums.ResultsUnit.Calories, Enums.EstimationFormula.KatchMcArdle, "5")]
        [DataRow(27, Enums.Gender.Female, "131", "51.1", Enums.ResultsUnit.Calories, Enums.EstimationFormula.KatchMcArdle, "72")]
        [DataRow(46, Enums.Gender.Male, "190.32", "201.55", Enums.ResultsUnit.Calories, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        [DataRow(18, Enums.Gender.Female, "96", "112.78", Enums.ResultsUnit.Calories, Enums.EstimationFormula.RevisedHarrisBenedict, "")]
        public void UserCan_CalculateBmrUsingDesiredSettings_InMetricUnits(int age, Enums.Gender gender, string heightInCm, string weightInKg, 
            Enums.ResultsUnit resultUnit, Enums.EstimationFormula estimationFormula, string bodyFatPercentage)
        {
            //Arrange

            //Act
            bmrCalculatorPage.EnterAge(age);
            bmrCalculatorPage.SelectGender(gender);
            bmrCalculatorPage.EnterFieldValue(Elements.heightInCmField, heightInCm);
            bmrCalculatorPage.EnterFieldValue(Elements.weightInKgField, weightInKg);

            bmrCalculatorPage.AdjustAllSettings(resultUnit, estimationFormula, bodyFatPercentage);
            var expectedBmrGeneralResult = bmrCalculatorPage.CalculateBmr(estimationFormula, age, gender, heightInCm, weightInKg, bodyFatPercentage);

            webDriverFacade.ClickElement(Elements.bmrCalculateButton);
            var actualBmrGeneralResult = webDriverFacade.GetElementText(Elements.bmrGeneralResult);
            var expectedBmrGeneralResultFinal = $"BMR = {expectedBmrGeneralResult} Calories/day";
           
            //Assert
            Assert.AreEqual(expectedBmrGeneralResultFinal, actualBmrGeneralResult.Replace("," , ""));
        }

        [DataTestMethod]
        [DataRow("72", Enums.Temperature.Celsius, Enums.Temperature.Fahrenheit, "161.6")]
        [DataRow("72", Enums.Temperature.Celsius, Enums.Temperature.Kelvin, "345.15")]
        [DataRow("72", Enums.Temperature.Celsius, Enums.Temperature.Celsius, "72")]
        [DataRow("678", Enums.Temperature.Kelvin, Enums.Temperature.Celsius, "404.85")]
        [DataRow("678", Enums.Temperature.Kelvin, Enums.Temperature.Fahrenheit, "760.73")]
        [DataRow("678", Enums.Temperature.Kelvin, Enums.Temperature.Kelvin, "678")]
        [DataRow("240", Enums.Temperature.Fahrenheit, Enums.Temperature.Kelvin, "388.70555556")]
        [DataRow("240", Enums.Temperature.Fahrenheit, Enums.Temperature.Celsius, "115.55555556")]
        [DataRow("240", Enums.Temperature.Fahrenheit, Enums.Temperature.Fahrenheit, "240")]
        public void UserCan_CalculateTemperatureConversion(string valueToBeConverted, Enums.Temperature temperatureToBeConvertedFrom,
            Enums.Temperature temperatureToBeConvertedTo, string expectedResult)
        {
            //Arrange
            var expectedConvertedValue = $"{temperatureToBeConvertedTo} ({expectedResult})";

            //Act
            var actualConvertedValue = bmrCalculatorPage.CalculateTemperatureConversion(valueToBeConverted, temperatureToBeConvertedFrom, temperatureToBeConvertedTo);

            //Assert
            Assert.AreEqual(expectedConvertedValue, actualConvertedValue);
        }

        [TestMethod]
        public void VerifyThat_TheToFieldOnTheOtherUnitsTabIsReadonly()
        {
            //Arrange
            var value = "224";
            var expectedResult = "";

            //Act
            webDriverFacade.ClickElement(Elements.bmrOtherUnitsTab);
            webDriverFacade.SwitchToIframe(0);
            webDriverFacade.TypeInput(Elements.bmrToField, value);
            var actualResult = webDriverFacade.GetElementText(Elements.bmrToField);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCleanup]
        public void TearDown()
        {
            webDriverFacade.CloseBrowser();
        }
    }
}
