﻿using Calculator.Net.pageobjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Firefox;
using System;

namespace Calculator.Net.pageobjectTests
{
    [TestClass]
    public class SalesTaxCalculatorPageTests_Firefox
    {
        #region Declarations
        WebDriverFacade webDriverFacade;
        SalesTaxCalculatorPage salesTaxCalculatorPage;
        #endregion

        [TestInitialize]
        public void Setup()
        {
            var firefoxOptions = new FirefoxOptions();
            firefoxOptions.AddArguments("incognito","--headless");
            webDriverFacade = new WebDriverFacade(new FirefoxDriver(firefoxOptions));
            //webDriverFacade = new WebDriverFacade(new FirefoxDriver());

            webDriverFacade.NavigateToUrl(Elements.salesTaxCalculatorPage_Url);
            webDriverFacade.WaitForPageToLoad(10);
            salesTaxCalculatorPage = new SalesTaxCalculatorPage(webDriverFacade);
        }

        [TestMethod]
        public void Verify_ResultHeading()
        {
            //Arrange
            var filename = "result-heading-";
            var expectedResultHeading = "Result";
            var expectedResultBkgrdColour = "rgb(81, 132, 40)"; //Unlike chrome, firefox ONLY return the rgb colour not rgba

            //Act
            salesTaxCalculatorPage.ClickCalculate();
            salesTaxCalculatorPage.ClickCalculate(); //had to do this twice because the page was refreshed, so the element couldn't be found
            var actualResultHeading = webDriverFacade.GetElementText(Elements.resultHeading);
            var actualResultBkgrdColour = webDriverFacade.GetElementCssValue(Elements.resultHeading, "background-color");
            webDriverFacade.TakeScreenshot(filename);

            //Assert
            Assert.AreEqual(expectedResultHeading, actualResultHeading);
            Assert.AreEqual(expectedResultBkgrdColour, actualResultBkgrdColour);
        }

        [DataTestMethod]
        [DataRow(3009, 19)]
        [DataRow(5854, 768.65)]
        [DataRow(0.09, 0.03)]
        public void UserCan_CalculateAfterTaxPrice(dynamic beforeTaxPrice, dynamic salesTaxRate)
        {
            //Arrange

            //Act
            salesTaxCalculatorPage.CalculateAfterTaxPrice(beforeTaxPrice.ToString(), salesTaxRate.ToString());
            var actualAfterTaxPriceResult = webDriverFacade.GetElementText(Elements.resultAfterTaxPrice);
            var saleTax = beforeTaxPrice * (salesTaxRate / 100.00);
            var expectedAfterTaxPriceResult = Math.Round(beforeTaxPrice + saleTax, 2);

            //Assert
            Assert.AreEqual("$" + expectedAfterTaxPriceResult.ToString(), actualAfterTaxPriceResult.Replace(",", "")); //the replace was added because the website returns the number like this: 12,000 and my calculation returns 12000
        }

        [TestCleanup]
        public void TearDown()
        {
            webDriverFacade.CloseBrowser();
        }
    }
}
