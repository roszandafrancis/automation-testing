﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static JNLiveOnlineBanking.Enum;

namespace JNLiveOnlineBanking.data
{
    public class UserDetails
    {
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Dictionary<string, string> AnswersForSecurityQuestion { get; set; }
        public ImageName ImageName { get; set; }
        public string TransactionPassword { get; set; }
        public int AccountNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneProvider { get; set; }
        public string PhoneCardAmount { get; set; }


        public static UserDetails ReadDataFromFile()
        {
            var filePath = ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "JNLiveOnlineBanking" + Path.DirectorySeparatorChar + "JNLiveOnlineBanking" + Path.DirectorySeparatorChar + "data" + Path.DirectorySeparatorChar + "UserDetails.json";
            var jsonString = File.ReadAllText(filePath);
            var userDetails = JsonConvert.DeserializeObject<UserDetails>(jsonString);

            return userDetails;
        }
    }
}
