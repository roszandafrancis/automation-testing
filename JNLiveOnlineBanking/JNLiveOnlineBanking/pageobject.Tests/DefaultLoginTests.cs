﻿using JNLiveOnlineBanking.data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject.Tests
{
    [TestClass]
    public class DefaultLoginTests
    {
        #region Declarations
        IWebDriver webDriver;
        readonly Browser browser = new Browser();
        UserDetails user = UserDetails.ReadDataFromFile();
        #endregion

        [TestInitialize]
        public void StartBrowser()
        {
            browser.OpenBrowser();
            //browser.OpenHeadlessChromeBrowser();
            webDriver = browser.webDriver;
        }

        [TestMethod]
        public void UserIsOnDefaultLoginPage_Success()
        {
            //Arrange
            var defaultLoginPage = new DefaultLoginPage(webDriver);

            //Act

            //Assert
            Assert.AreEqual(defaultLoginPage.url, webDriver.Url);
        }
        
        [TestMethod]
        public void UserLogin_Success()
        {
            //Arrange
            var defaultLoginPage = new DefaultLoginPage(webDriver);

            //Act
            defaultLoginPage.DefaultLogin(user.Username, user.Password);

            //Assert
            Assert.AreNotEqual(defaultLoginPage.url, webDriver.Url);
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            browser.CloseBrowser();
        }
    }
}
