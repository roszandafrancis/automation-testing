﻿using JNLiveOnlineBanking.data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject.Tests
{
    [TestClass]
    public class SecurityQuestionLoginPageTests
    {
        #region Declarations
        IWebDriver webDriver;
        readonly Browser browser = new Browser();
        readonly UserDetails user = UserDetails.ReadDataFromFile();
        #endregion

        [TestInitialize]
        public void StartBrowser()
        {
            browser.OpenBrowser();
            //browser.OpenHeadlessChromeBrowser();
            webDriver = browser.webDriver;

            var defaultLoginPage = new DefaultLoginPage(webDriver);
            defaultLoginPage.DefaultLogin(user.Username, user.Password);
        }

        [TestMethod]
        public void UserIsOnSecurityQuestionLoginPage_Success()
        {
            //Arrange
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);

            //Act
            var nameOfUser = webDriver.FindElement(securityQuestionLoginPage.userInformation).Text;

            //Assert
            Assert.AreEqual(securityQuestionLoginPage.url, webDriver.Url);
            Assert.IsTrue(nameOfUser.Contains(user.FullName));

        }
        
        [TestMethod]
        public void AnswerSecurityQuestion_Success()
        {
            //Arrange
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);

            //Act
            securityQuestionLoginPage.AnswerSecurityQuestion(user.AnswersForSecurityQuestion);

            //Assert
            Assert.AreNotEqual(securityQuestionLoginPage.url, webDriver.Url);
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            browser.CloseBrowser();
        }
    }
}
