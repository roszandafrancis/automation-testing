﻿using JNLiveOnlineBanking.data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject.Tests
{
    [TestClass]
    public class PhoneTopupPageTests
    {
        #region Declarations
        IWebDriver webDriver;
        readonly Browser browser = new Browser();
        readonly UserDetails user = UserDetails.ReadDataFromFile();
        #endregion

        [TestInitialize]
        public void StartBrowser()
        {
            browser.OpenBrowser();
            //browser.OpenHeadlessChromeBrowser();
            webDriver = browser.webDriver;

            #region Variable Initalizations
            var defaultLoginPage = new DefaultLoginPage(webDriver);
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);
            var authenticationImageLoginPage = new AuthenticationImageLoginPage(webDriver);
            var dashboard = new DashboardPage(webDriver);
            #endregion

            defaultLoginPage.DefaultLogin(user.Username, user.Password);
            securityQuestionLoginPage.AnswerSecurityQuestion(user.AnswersForSecurityQuestion);
            authenticationImageLoginPage.SelectSecurityImage(user.ImageName);
            dashboard.SelectPhoneTopUpOption();
        }

        [TestMethod]
        public void TopUpMobilePhone_Success()
        {
            //Arrange
            var phoneTopupPage = new PhoneTopupPage(webDriver);

            //Act
            phoneTopupPage.TopUpMobilePhone(user.TransactionPassword, user.PhoneNumber, user.PhoneProvider, user.PhoneCardAmount);

            //Assert
            Assert.AreNotEqual(phoneTopupPage.url, webDriver.Url);
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            browser.CloseBrowser();
        }
    }
}
