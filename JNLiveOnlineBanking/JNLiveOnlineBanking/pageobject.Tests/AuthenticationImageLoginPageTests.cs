﻿using JNLiveOnlineBanking.data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject.Tests
{
    [TestClass]
    public class AuthenticationImageLoginPageTests
    {
        #region Declarations
        IWebDriver webDriver;
        readonly Browser browser = new Browser();
        readonly UserDetails user = UserDetails.ReadDataFromFile();
        #endregion

        [TestInitialize]
        public void StartBrowser()
        {
            browser.OpenBrowser();
            //browser.OpenHeadlessChromeBrowser();
            webDriver = browser.webDriver;

            #region Variable Initalizations
            var defaultLoginPage = new DefaultLoginPage(webDriver);
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);
            #endregion

            defaultLoginPage.DefaultLogin(user.Username, user.Password);
            securityQuestionLoginPage.AnswerSecurityQuestion(user.AnswersForSecurityQuestion);
        }

        [TestMethod]
        public void UserIsOnAuthenticationImageLoginPage_Success()
        {
            //Arrange
            var authenticationImageLoginPage = new AuthenticationImageLoginPage(webDriver);

            //Act

            //Assert
            Assert.AreEqual(authenticationImageLoginPage.url, webDriver.Url);
        }

        [TestMethod]
        public void ChooseImage_Success()
        {
            //Arrange
            var authenticationImageLoginPage = new AuthenticationImageLoginPage(webDriver);

            //Act
            authenticationImageLoginPage.SelectSecurityImage(user.ImageName);

            //Assert
            Assert.AreNotEqual(authenticationImageLoginPage.url, webDriver.Url);
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            browser.CloseBrowser();
        }
    }
}
