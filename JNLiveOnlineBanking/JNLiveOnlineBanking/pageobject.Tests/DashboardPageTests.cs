﻿using JNLiveOnlineBanking.data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject.Tests
{
    [TestClass]
    public class DashboardPageTests
    {
        #region Declarations
        IWebDriver webDriver;
        readonly Browser browser = new Browser();
        readonly UserDetails user = UserDetails.ReadDataFromFile();
        #endregion

        [TestInitialize]
        public void StartBrowser()
        {
            browser.OpenBrowser();
            //browser.OpenHeadlessChromeBrowser();
            webDriver = browser.webDriver;

            #region Variable initalizations
            var defaultLoginPage = new DefaultLoginPage(webDriver);
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);
            var authenticationImageLoginPage = new AuthenticationImageLoginPage(webDriver);
            #endregion

            defaultLoginPage.DefaultLogin(user.Username, user.Password);
            securityQuestionLoginPage.AnswerSecurityQuestion(user.AnswersForSecurityQuestion);
            authenticationImageLoginPage.SelectSecurityImage(user.ImageName);
        }

        [TestMethod]
        public void UserIsOnDashboardPage_Success()
        {
            //Arrange
            var dashboard = new DashboardPage(webDriver);

            //Act

            //Assert
            Assert.AreEqual(dashboard.url, webDriver.Url);
        }

        [TestMethod]
        public void SelectPhoneTopup_Success()
        {
            //Arrange
            var dashboard = new DashboardPage(webDriver);

            //Act
            dashboard.SelectPhoneTopUpOption();

            //Assert
            Assert.AreNotEqual(dashboard.url, webDriver.Url);
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            browser.CloseBrowser();
        }
    }
}
