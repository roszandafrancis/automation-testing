﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking
{
    public class Browser
    {
        public IWebDriver webDriver;
        private readonly string filePath = ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "JNLiveOnlineBanking" + Path.DirectorySeparatorChar + "JNLiveOnlineBanking" + Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "Debug";

        /// <summary>
        /// Goes straight to the index page
        /// </summary>
        public void OpenBrowser()
        {
            //webDriver = new ChromeDriver(filePath);
            webDriver = new ChromeDriver(filePath);
            webDriver.Navigate().GoToUrl("https://www.jnbslive.com/Default.aspx");
            webDriver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Goes to the specified url
        /// </summary>
        /// <param name="url">the url that you want to load when the browsser opens</param>
        public void OpenBrowser(string url)
        {
            webDriver = new ChromeDriver(filePath);
            webDriver.Navigate().GoToUrl(url);
            webDriver.Manage().Window.Maximize();
        }

        public void OpenHeadlessChromeBrowser()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");

            webDriver = new ChromeDriver(filePath, chromeOptions);
            webDriver.Navigate().GoToUrl("https://www.jnbslive.com/Default.aspx");
        }

        /// <summary>
        /// Goes to the specified url
        /// </summary>
        /// <param name="url">the url that you want to load when the browsser opens</param>
        public void OpenHeadlessChromeBrowser(string url)
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");

            webDriver = new ChromeDriver(filePath, chromeOptions);
            webDriver.Navigate().GoToUrl(url);
        }

        public void CloseBrowser()
        {
            webDriver.Quit();
        }
    }
}
