﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking
{
    public class Enum
    {
        public enum ImageName
        {
            PottedFlower,
            Lock,
            Leaf,
            Key,
            Cards,
            Basketball,
            Lamp,
            Clock,
            Fish,
            PaperFan,
            Bucket
        }
    }
}
