﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject
{
    public class DefaultLoginPage
    {
        readonly IWebDriver webDriver;
        
        #region Public Element Declarations
        public readonly string url = "https://www.jnbslive.com/Default.aspx";
        #endregion

        #region Private Element Declarations
        readonly By usernameField = By.Id("MainContent_txtUser");
        readonly By passwordField = By.Id("MainContent_txtPassword");
        readonly By signInButton = By.Id("MainContent_btnSubmit");
        readonly By forgotCredentialsLink = By.Id("MainContent_lnkForgotPassword");
        readonly By registrationLink = By.Id("myHeader_lnkRegister");
        readonly By unlockUserLink = By.LinkText("/Pages/CredentialRecovery.aspx?Unlock=21&Username=RosieFranz200");
        #endregion


        public DefaultLoginPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void DefaultLogin(string username, string password)
        {
            EnterUsername(username);
            EnterPassword(password);
            ClickSignIn();
        }

        public void EnterUsername(string username)
        {
            webDriver.FindElement(usernameField).SendKeys(username);
        }
        
        public void EnterPassword(string password)
        {
            webDriver.FindElement(passwordField).SendKeys(password);
        }
        public void ClickSignIn()
        {
            webDriver.FindElement(signInButton).Click();
        }

    }
}
