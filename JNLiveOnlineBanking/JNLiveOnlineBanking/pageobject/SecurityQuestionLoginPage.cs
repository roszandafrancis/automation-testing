﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject
{
    public class SecurityQuestionLoginPage
    {
        readonly IWebDriver webDriver;

        #region Private Element Declarations
        readonly By securityQuestionLabel = By.XPath("/html/body/form/div[3]/div/div[1]/div[2]/div[2]/div[1]/div[2]/table/tbody/tr[1]/td/span");
        readonly By securityAnswerField = By.XPath("/html/body/form/div[3]/div/div[1]/div[2]/div[2]/div[1]/div[2]/table/tbody/tr[2]/td/input[2]");
        readonly By confirmButton = By.Id("MainContent_btnConfirm");
        #endregion

        #region Public Element Declarations
        public readonly By userInformation = By.Id("MainContent_lblUser");
        public readonly string url = "https://www.jnbslive.com/LoginAuthentication.aspx";
        #endregion

        public SecurityQuestionLoginPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        /// <summary>
        /// Used to answer the security question asked by the site
        /// </summary>
        /// <param name="answer">the key is a unique word from the security question. 
        /// The value is the exact case sensitive answer to the security question</param>
        public void AnswerSecurityQuestion(Dictionary<string, string> answer)
        {
            var securityQuestion = webDriver.FindElement(securityQuestionLabel).Text;
            var flag = 0;

            foreach(var keyword in answer)
            {
                if (securityQuestion.Contains(keyword.Key))
                {
                    webDriver.FindElement(securityAnswerField).SendKeys(keyword.Value);
                    webDriver.FindElement(confirmButton).Click();
                    webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
                    flag++;
                    break;
                }
            }
            if (flag == 0)
            {
                throw new Exception("The Security question did not contain the keyword provided");
            }
        }

    }
}
