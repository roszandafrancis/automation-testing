﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject
{
    public class PhoneTopupPage
    {
        readonly IWebDriver webDriver;

        #region Public Element Declarations
        public readonly string url = "https://www.jnbslive.com/Pages/PhoneTopUp.aspx";
        #endregion

        #region Private Element Declarations
        readonly By transactionPasswordField = By.Id("MainContent_PasswordInput");
        readonly By transactionPasswordConfirmButton = By.Id("MainContent_PasswordVerificationButton");
        readonly By desiredAccountCheckbox = By.XPath("/html/body/form/div[3]/div/div[1]/div/section/div[4]/section/div/table/tbody/tr[3]/td[1]/label");
        readonly By existingPhoneNumberDDL = By.Id("MainContent_LastPhoneNumberToppedUpDropDownList");
        readonly By addNewPhoneNumberCheckbox = By.XPath("/html/body/form/div[3]/div/div[1]/div/section/div[4]/div[2]/div[2]/div[2]/label");
        readonly By addNewPhoneNumberField = By.Id("MainContent_PhoneNumber");
        readonly By phoneProviderDDL = By.Id("MainContent_PhoneProviderDropDownList");
        readonly By phoneCardTypeDDL = By.Id("MainContent_PhoneCardTypeDropDownList");
        readonly By confirmButton = By.Id("MainContent_ButtonSubmit");
        #endregion

        public PhoneTopupPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void TopUpMobilePhone(string password, string phoneNumber, string phoneProvider, string phoneCardAmount)
        {
            EnterTransactionPassword(password);
            SelectPaymentAccount();
            SelectOrAddPhoneNumber(phoneNumber);
            SelectPhoneProvider(phoneProvider);
            SelectPhoneCardType(phoneCardAmount);
            ConfirmPurchase();
        }

        private void ConfirmPurchase()
        {
            //Submit button to confirm purchase details
            webDriver.FindElement(confirmButton).Click();
            
            //Submit button after confirming details
            webDriver.FindElement(confirmButton).Click();
        }

        private void SelectPaymentAccount()
        {
            webDriver.FindElement(desiredAccountCheckbox).Click();
        }

        public void EnterTransactionPassword(string password)
        {
            webDriver.FindElement(transactionPasswordField).SendKeys(password);
            webDriver.FindElement(transactionPasswordConfirmButton).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }

        private void SelectOrAddPhoneNumber(string phoneNumber)
        {
            var flag = SelectPhoneNumber(phoneNumber);

            if (flag == 0)
            {
                AddPhoneNumberIfDoesntExist(phoneNumber);
            }
        }

        public int SelectPhoneNumber(string phoneNumber)
        {
            var flag = 0;
            var phoneDDL = webDriver.FindElement(existingPhoneNumberDDL);
            var selectElement = new SelectElement(phoneDDL);

            foreach (var element in selectElement.Options)
            {
                if (element.Text == phoneNumber)
                {
                    element.Click();
                    flag = 1;
                    break;
                }
            }
            return flag;
        }


        private void AddPhoneNumberIfDoesntExist(string phoneNumber)
        {
            webDriver.FindElement(addNewPhoneNumberCheckbox).Click();
            webDriver.FindElement(addNewPhoneNumberField).SendKeys(phoneNumber);
        }

        public void SelectPhoneProvider(string phoneProvider)
        {
            var providerDDL = webDriver.FindElement(phoneProviderDDL);
            var selectElement = new SelectElement(providerDDL);
            selectElement.SelectByText(phoneProvider);
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }

        public void SelectPhoneCardType(string phoneCardAmount)
        {
            var cardDDL = webDriver.FindElement(phoneCardTypeDDL);
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);

            //Because the page was refreshed the DOM change and the element became 'stale', 
            //so have to Find the 'phoneCardTypeDDL' Element again
            cardDDL = webDriver.FindElement(phoneCardTypeDDL);
            var selectElement = new SelectElement(cardDDL);
            selectElement.SelectByText(phoneCardAmount, true); //the true is for a partial instead of an exact match
        }
    }
}
