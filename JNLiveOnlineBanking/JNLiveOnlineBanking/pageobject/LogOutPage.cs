﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject
{
    public class LogOutPage
    {
        readonly IWebDriver webDriver;

        #region Element Declarations
        public readonly string url = "https://www.jnbslive.com/Pages/PhoneTopUp.aspx";
        #endregion

        #region Private Element Declarations
        readonly By logoutMenuButton = By.ClassName("user_btn");
        readonly By logoutButton = By.Id("myHeader_lnkLogOut");
        #endregion

        public LogOutPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void Logout()
        {
            webDriver.FindElement(logoutMenuButton).Click();
            webDriver.FindElement(logoutButton).Click();
        }
    }
}
