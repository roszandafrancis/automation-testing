﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.pageobject
{
    public class DashboardPage
    {
        readonly IWebDriver webDriver;

        #region Public Element Declarations
        public readonly string url = "https://www.jnbslive.com/Pages/Dashboard.aspx";
        #endregion

        #region Private Element Declarations
        //readonly By phoneTopupBox = By.XPath("//*[contains(text(), 'Phone Top-Up')]");
        readonly By logoButton = By.Id("myHeader_lnkHome");
        readonly By phoneTopupIcon = By.Id("myMenu_lnkPhone TopUp_Col");
        #endregion

        public DashboardPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void SelectPhoneTopUpOption()
        {
            ClickLogo();
            SelectPhoneTopupOption();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
        }

        private void SelectPhoneTopupOption()
        {
            webDriver.FindElement(phoneTopupIcon).Click(); //to open hamburger menu
            webDriver.FindElement(phoneTopupIcon).Click(); //to click the actual icon
        }

        private void ClickLogo()
        {
            webDriver.FindElement(logoButton).Click();
        }
    }
}
