﻿using JNLiveOnlineBanking.data;
using JNLiveOnlineBanking.pageobject;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.Console
{
    public class PurchasePhoneCard
    {
        static void Main(string[] args)
        {
            #region Declarations
            IWebDriver webDriver;

            var user = UserDetails.ReadDataFromFile();
            var browser = new Browser();
            browser.OpenBrowser();

            webDriver = browser.webDriver;
            var helper = new HelperClass(webDriver);
            var phoneTopupPage = new PhoneTopupPage(webDriver);
            var dashboard = new DashboardPage(webDriver);
            #endregion

            helper.LoginStepsToDashBoard();
            dashboard.SelectPhoneTopUpOption();
            phoneTopupPage.TopUpMobilePhone(user.TransactionPassword, user.PhoneNumber, user.PhoneProvider, user.PhoneCardAmount);
            helper.LogOutFromApplication();
        }

        
    }
}
