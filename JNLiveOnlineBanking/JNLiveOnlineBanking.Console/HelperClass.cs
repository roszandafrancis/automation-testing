﻿using JNLiveOnlineBanking.data;
using JNLiveOnlineBanking.pageobject;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JNLiveOnlineBanking.Console
{
    public class HelperClass
    {
        readonly IWebDriver webDriver;
        readonly UserDetails user = UserDetails.ReadDataFromFile();


        public HelperClass(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public void LogOutFromApplication()
        {
            var logOutPage = new LogOutPage(webDriver);
            logOutPage.Logout();
        }

        public void LoginStepsToDashBoard()
        {
            #region Variable Initalizaions
            var defaultLoginPage = new DefaultLoginPage(webDriver);
            var securityQuestionLoginPage = new SecurityQuestionLoginPage(webDriver);
            var authenticationImageLoginPage = new AuthenticationImageLoginPage(webDriver);
            #endregion

            defaultLoginPage.DefaultLogin(user.Username, user.Password);
            securityQuestionLoginPage.AnswerSecurityQuestion(user.AnswersForSecurityQuestion);
            authenticationImageLoginPage.SelectSecurityImage(user.ImageName);
        }
    }
}
